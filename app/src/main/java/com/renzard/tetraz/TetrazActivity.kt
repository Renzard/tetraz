package com.renzard.tetraz

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.renzard.tetraz.storage.AppPreferences

class TetrazActivity : AppCompatActivity() {

    var screen_high_score: TextView? = null
    var screen_current_score: TextView? = null
    var appPreferences: AppPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tetraz)
        supportActionBar?.hide()
        appPreferences = AppPreferences(this)


        //restart Button
        val button_restart = findViewById<Button>(R.id.button_restart)

        //scores
        screen_high_score = findViewById(R.id.screen_high_score)
        screen_current_score = findViewById(R.id.screen_current_score)

        updateHighScore()
        updateCurrentScore()
    }

    private fun updateHighScore() {
        screen_high_score?.text = "${appPreferences?.getHighScore()}"
    }

    private fun updateCurrentScore() {
        screen_current_score?.text = "0"
    }
}
