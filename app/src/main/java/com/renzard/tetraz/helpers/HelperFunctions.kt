package com.renzard.tetraz.helpers

fun array2dHelper(sizeOuter: Int, sizeInner: Int): Array<ByteArray> =
    Array(sizeOuter) { ByteArray(sizeInner) }