package com.renzard.tetraz

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.renzard.tetraz.storage.AppPreferences
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //action Bar
        supportActionBar?.hide()

        //new game button
        val buttonNewGame = findViewById<Button>(R.id.button_new_game)
        buttonNewGame.setOnClickListener(this::newGameEvent)

        //reset button
        val buttonReset = findViewById<Button>(R.id.button_reset_score)
        buttonReset.setOnClickListener(this::resetScoreEvent)

        //exit button
        val buttonExit = findViewById<Button>(R.id.button_exit)
        buttonExit.setOnClickListener(this::exitEvent)

        //screen Score Update
        val screenHighScore = findViewById<TextView>(R.id.screen_high_score)
    }

    private fun newGameEvent(view: View) {
        val intent = Intent(this, TetrazActivity::class.java)
        startActivity(intent)
    }

    private fun resetScoreEvent(view: View) {
        val preferences = AppPreferences(this)
        preferences.clearHighScore()
        Snackbar.make(view, "Score has been reset", Snackbar.LENGTH_SHORT).show()
        screen_high_score.text = "High score: ${preferences.getHighScore()}"
    }


    private fun exitEvent(view: View) {
        exitProcess(0)
    }
}
